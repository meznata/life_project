package com.uca.dao;

import java.sql.*;

public class _Initializer {
    // Table name for the grid
    private static final String TABLE_NAME = "plateau";
    // Grid size
    private static final int GRID_SIZE = 100;

    /**
     * Initializes the grid by creating a table if it doesn't exist
     */
    public static void _initialize() {
        Connection connection = _Connector.getMainConnection();

        try {
            if (!doesTableExist(connection, TABLE_NAME)) {
                PreparedStatement createTableStatement = connection.prepareStatement(
                        "CREATE TABLE " + TABLE_NAME + " (x INT NOT NULL, y INT NOT NULL, state BOOLEAN NOT NULL, PRIMARY KEY(x, y));"
                );
                createTableStatement.executeUpdate();

                for (int i = 0; i < GRID_SIZE; i++) {
                    for (int j = 0; j < GRID_SIZE; j++) {
                        PreparedStatement insertStatement = connection.prepareStatement(
                                "INSERT INTO " + TABLE_NAME + " (x, y, state) VALUES (?, ?, false);"
                        );
                        insertStatement.setInt(1, i);
                        insertStatement.setInt(2, j);
                        insertStatement.executeUpdate();
                    }
                }
            }

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if a table exists in the database
     */
    private static boolean doesTableExist(Connection connection, String tableName) throws SQLException {
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet resultSet = metaData.getTables(null, null, tableName, new String[]{"TABLE"});
        return resultSet.next();
    }

    /**
     * Rolls back the changes made in the given connection
     */
    public static void rollback(Connection connection) {
        try {
            connection.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Commits the changes made in the given connection
     */
    public static void commit(Connection connection) {
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

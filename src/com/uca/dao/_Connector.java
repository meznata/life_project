package com.uca.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;


    public class _Connector {

        private static final String url = "jdbc:postgresql://localhost:5432/life";
        private static final String user = "mehdiznata";
        private static final String password = "";
        private static Connection connect;

        private static final Map<Integer, Connection> connections = new HashMap<>();

        public static Connection getConnection(Integer id) {
            Connection connection = connections.get(id);
            if (connection == null) {
                connection = createNewConnection();
                connections.put(id, connection);
            }
            return connection;
        }

        public static Connection getMainConnection() {
            if (connections.isEmpty()) {
                return createNewConnection();
            }
            return connections.values().iterator().next();
        }

        private static Connection createNewConnection() {
            Connection connection;
            try {
                connection = DriverManager.getConnection(url, user, password);
                connection.setAutoCommit(false);
                connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            } catch (SQLException e) {
                System.err.println("Error creating a new connection.");
                throw new RuntimeException(e);
            }
            return connection;
        }
    }

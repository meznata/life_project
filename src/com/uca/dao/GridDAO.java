package com.uca.dao;

import java.sql.*;
import com.uca.entity.*;
import java.util.List;
import java.util.ArrayList;

public class GridDAO {
    // Method to retrieve the list of living cells
    public static List<CellEntity> getLivingCells(Connection connection) {
        List<CellEntity> livingCells = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT x, y FROM plateau WHERE state = true;"
            );
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int x = resultSet.getInt("x");
                int y = resultSet.getInt("y");
                CellEntity cell = new CellEntity(x, y);
                livingCells.add(cell);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return livingCells;
    }

    // Method to get the state of a given cell
    private boolean getState(CellEntity cell, Connection connection) {
        boolean state = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT state FROM plateau WHERE x = ? AND y = ?;"
            );
            preparedStatement.setInt(1, cell.getX());
            preparedStatement.setInt(2, cell.getY());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                state = resultSet.getBoolean("state");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return state;
    }

    // Toggle the state of a cell when clicked, showing/hiding red dots
    public void changeState(CellEntity selectedCell, Connection connection) {
        boolean currentState = getState(selectedCell, connection);
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE plateau SET state = ? WHERE x = ? AND y = ?;"
            );
            preparedStatement.setBoolean(1, !currentState);
            preparedStatement.setInt(2, selectedCell.getX());
            preparedStatement.setInt(3, selectedCell.getY());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Set the state of each cell to false in the database, clearing the grid
    public void clearGrid(Connection connection) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE plateau SET state = false;"
            );
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to change the state of the grid based on a given list of living cells
    // It resets the database by setting all cells to false and then sets the given cells to true
    public void changeGrid(Connection connection, List<CellEntity> livingCells) {
        try {
            // Clear the grid
            clearGrid(connection);

            // Update the state of the living cells
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE plateau SET state = true WHERE x = ? AND y = ?;"
            );
            for (CellEntity cell : livingCells) {
                preparedStatement.setInt(1, cell.getX());
                preparedStatement.setInt(2, cell.getY());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to simulate each step of the game of life
    // It generates a 2D array with the number of living neighbors for each cell
    // Then it updates the database based on the game of life rules
    public void nextGeneration(Connection connection) {
        try {
            // Create a temporary table to store the next generation state
            PreparedStatement createTempTableStatement = connection.prepareStatement(
                    "CREATE TEMPORARY TABLE next_gen AS SELECT * FROM plateau;"
            );
            createTempTableStatement.executeUpdate();

            // Update the state of each cell in the temporary table based on the game of life rules
            PreparedStatement updateStatement = connection.prepareStatement(
                    "UPDATE next_gen SET state = ? WHERE x = ? AND y = ?;"
            );
            for (int x = 0; x < 100; x++) {
                for (int y = 0; y < 100; y++) {
                    int livingNeighbors = countLivingNeighbors(connection, x, y);
                    boolean currentState = getState(new CellEntity(x, y), connection);
                    boolean nextState = determineNextState(currentState, livingNeighbors);
                    updateStatement.setBoolean(1, nextState);
                    updateStatement.setInt(2, x);
                    updateStatement.setInt(3, y);
                    updateStatement.executeUpdate();
                }
            }

            // Update the grid with the next generation state
            PreparedStatement updateGridStatement = connection.prepareStatement(
                    "UPDATE plateau , next_gen ng SET g.state = ng.state WHERE g.x = ng.x AND g.y = ng.y;"
            );
            updateGridStatement.executeUpdate();

            // Drop the temporary table
            PreparedStatement dropTempTableStatement = connection.prepareStatement(
                    "DROP TABLE next_gen;"
            );
            dropTempTableStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to count the number of living neighbors for a given cell
    private int countLivingNeighbors(Connection connection, int x, int y) {
        int count = 0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT COUNT(*) AS count FROM plateau WHERE state = true AND (x BETWEEN ?-1 AND ?+1) AND (y BETWEEN ?-1 AND ?+1) AND NOT (x = ? AND y = ?);"
            );
            preparedStatement.setInt(1, x);
            preparedStatement.setInt(2, x);
            preparedStatement.setInt(3, y);
            preparedStatement.setInt(4, y);
            preparedStatement.setInt(5, x);
            preparedStatement.setInt(6, y);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt("count");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    // Method to determine the next state of a cell based on the current state and the number of living neighbors
    private boolean determineNextState(boolean currentState, int livingNeighbors) {
        if (currentState) {
            // Any live cell with fewer than two live neighbors dies
            // Any live cell with more than three live neighbors dies
            return (livingNeighbors >= 2 && livingNeighbors <= 3);
        } else {
            // Any dead cell with exactly three live neighbors becomes a live cell
            return (livingNeighbors == 3);
        }
    }
}

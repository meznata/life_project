package com.uca.entity;

import java.util.List;
import java.util.ArrayList;

/**
 * Represente une cellule de coordonné x et y
*/
public class CellEntity {
    private final int x;
    private final int y;
    private boolean state;

    public boolean isState() {
        return state;
    }

    public CellEntity(int x, int y,boolean state) {
        this.x = x;
        this.y = y;
        this.state = state;
    }

    public CellEntity(int x, int y) {
        this(x, y, false);
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }


    public void setState(boolean state) {
        this.state = state;
    }

    public String toString() {
        return x+","+y;
    }

    public int getlivingNeighbors() {
        List<CellEntity> cells = new ArrayList<>() ;
        int livingNeighbors = 0;
        for (CellEntity cell : cells) {
            if (Math.abs(cell.getX() - x) <= 1 && Math.abs(cell.getY() - y) <= 1 && cell.isState()) {
                livingNeighbors++;
            }
        }
        return livingNeighbors;
    }  // added this function 

    public void incrementLivingNeighbors() {
    }
}

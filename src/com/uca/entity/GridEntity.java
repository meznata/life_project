package com.uca.entity;

import com.uca.dao.GridDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

/**
 * Représente l'état de la grille
 * Toutes les cellules de l'état de la grille sont vivantes
 */
public class GridEntity {
    // état de la grille : liste des cellules vivantes
    private final List<CellEntity> state;

    public GridEntity() {
        state = new ArrayList<>();
    }

    /**
     * Retourne la grille
     */
    public List<CellEntity> getCells() {
        return state;
    }

    /**
     * Ajoute une cellule vivante à la grille
     */
    public void addCell(CellEntity c) {
        state.add(c);
    }

    /**
     * Supprime toutes les cellules de la grille
     */
    public void clearGrid() {
        state.clear();
    }


    /**
     * Récupère les cellules de la grille depuis la base de données pour la session donnée
     * @param session - numéro de session
     */
    public void getGrid(Connection session) throws SQLException {
        state.clear();
        state.addAll(GridDAO.getLivingCells(session));
    }


    /**
     * Calcule la prochaine génération de la grille en fonction de l'état actuel
     * @param currentGrid - grille actuelle
     * @return GridEntity représentant la prochaine génération de la grille
     */
    public static GridEntity calculateNextGeneration(GridEntity currentGrid) {
        GridEntity nextGrid = new GridEntity();
        List<CellEntity> currentCells = currentGrid.getCells();

        // Créer une copie temporaire de la grille actuelle
        List<CellEntity> tempCells = new ArrayList<>(currentCells);

        // Parcourir toutes les cellules de la grille actuelle
        for (CellEntity currentCell : currentCells) {
            int x = currentCell.getX();
            int y = currentCell.getY();

            // Vérifier les cellules voisines
            for (int i = x - 1; i <= x + 1; i++) {
                for (int j = y - 1; j <= y + 1; j++) {
                    // Ignorer la cellule courante
                    if (i == x && j == y) {
                        continue;
                    }

                    // Vérifier si la cellule voisine est vivante
                    CellEntity neighbor = new CellEntity(i, j);
                    if (currentCells.contains(neighbor)) {
                        // Incrémenter le compteur de voisins vivants
                        currentCell.incrementLivingNeighbors();
                    } else {
                        // Ajouter la cellule voisine à la copie temporaire si elle n'est pas déjà présente
                        if (!tempCells.contains(neighbor)) {
                            tempCells.add(neighbor);
                        }
                    }
                }
            }
        }

        // Appliquer les règles du jeu de la vie pour chaque cellule de la copie temporaire
        for (CellEntity tempCell : tempCells) {
            int livingNeighbors = tempCell.getlivingNeighbors();

            // Règle 1: Une cellule morte avec exactement 3 voisins vivants devient une cellule vivante
            if (!currentCells.contains(tempCell) && livingNeighbors == 3) {
                nextGrid.addCell(tempCell);
            }

            // Règle 2: Une cellule vivante avec moins de 2 ou plus de 3 voisins vivants meurt
            if (currentCells.contains(tempCell) && (livingNeighbors < 2 || livingNeighbors > 3)) {
                // Ne pas ajouter la cellule à la prochaine génération
                continue;
            }

            // Règle 3: Une cellule vivante avec 2 ou 3 voisins vivants reste vivante dans la prochaine génération
            if (currentCells.contains(tempCell) && (livingNeighbors == 2 || livingNeighbors == 3)) {
                nextGrid.addCell(tempCell);
            }
        }
        return null;
    }

    public void saveCells(List<CellEntity> cells) {
        state.addAll(cells);
    }




}

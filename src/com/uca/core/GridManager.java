package com.uca.core;

import com.uca.dao.GridDAO;
import com.uca.dao._Connector;
import com.uca.entity.CellEntity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GridManager {
    private int session;
    private static Connection connection;

    public GridManager(int session) {
        this.session = session;
        this.connection = _Connector.getConnection(session);
    }

    public static List<CellEntity> getGrid() throws SQLException {
        GridDAO gridDAO = new GridDAO();
        return gridDAO.getLivingCells(connection);
    }

    public void changeState(CellEntity selectedCell) throws SQLException {
        GridDAO gridDAO = new GridDAO();
        gridDAO.changeState(selectedCell, connection);
    }

    public void clearGrid() {
        GridDAO gridDAO = new GridDAO();
        gridDAO.clearGrid(connection);
    }

    public void changeGrid(List<CellEntity> state) {
        GridDAO gridDAO = new GridDAO();
        gridDAO.changeGrid(connection, state);
    }

    public void simulateNextGeneration() {
        GridDAO gridDAO = new GridDAO();
        gridDAO.nextGeneration(connection);
    }
    /**
     * Décode le contenu d'un fichier RLE sous forme de cases à partir d'un URL
     * @param url - url d'un fichier RLE, ex : https://www.conwaylife.com/patterns/glider.rle
     */
    public static List<CellEntity> decodeRLEUrl(String url) throws Exception {
        URL u = new URL(url);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(u.openStream()));

        StringBuffer sb = new StringBuffer();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            sb.append(inputLine);
            System.out.println(inputLine);
            sb.append("\n");
        }

        in.close();

        return decodeRLE(sb.toString());
    }

    /**
     * Décode le contenu d'un fichier RLE sous forme de cases
     * @param rle - un chaîne représentant une serialisation RLE
     */
    public static List<CellEntity> decodeRLE(String rle) {
        List<CellEntity> cells = new ArrayList<>();
        boolean ignore = false;
        int step = 1;
        int x = 50;
        int y = 50;
        String number;
        Pattern pattern = Pattern.compile("^[0-9]+");
        int i = -1;
        while (i < rle.length() - 1) {
            i++;
            if (ignore) {
                if (rle.charAt(i) == '\n') {
                    ignore = false;
                }
                continue;
            }
            switch (rle.charAt(i)) {
                case '#':
                case 'x':
                case '!':
                    ignore = true;
                    continue;
                case '$':
                    x = 50;
                    y += step;
                    step = 1;
                    continue;
                case 'b':
                    x += step;
                    step = 1;
                    continue;
                case 'o':
                    for (int j = 0; j < step; j++) {
                        CellEntity c = new CellEntity(x++, y);
                        System.out.println(c);
                        cells.add(c);
                    }
                    System.out.println(rle.substring(Math.max(0, rle.lastIndexOf("$",i))));
                    step = 1;
                    continue;
            }
            Matcher matcher = pattern.matcher(rle.substring(i));
            if (matcher.find()) {
                number = matcher.group();
                step = Integer.parseInt(number);
                i += number.length() - 1;
            }
        }
        return cells;
    }
}




package com.uca;

import com.google.gson.Gson;
import com.uca.core.GridManager;
import com.uca.dao._Connector;
import com.uca.dao._Initializer;
import com.uca.entity.CellEntity;
import com.uca.gui.IndexGUI;
import spark.Request;
import spark.Response;

import java.util.List;

import static spark.Spark.*;

public class StartServer {

    public static void main(String[] args) {
        // Configuration de Spark
        staticFiles.location("/static/");
        port(8081);

        // Création de la base de données, si besoin
        _Initializer._initialize();

        /**
         * Définition des routes
         */

        // index de l'application
        get("/", (req, res) -> {
            return IndexGUI.getIndex();
        });

        // retourne l'état de la grille
        get("/grid", (req, res) -> {
            res.type("application/json");
            int numSession = getSession(req);
            GridManager gridManager = new GridManager(numSession);
            return new Gson().toJson(gridManager.getGrid());
        });

        // inverse l'état d'une cellule
        put("/grid/change", (req, res) -> {
            Gson gson = new Gson();
            CellEntity selectedCell = gson.fromJson(req.body(), CellEntity.class);
            int numSession = getSession(req);
            GridManager gridManager = new GridManager(numSession);
            gridManager.changeState(selectedCell);

            return new Gson().toJson(gridManager.getGrid());
        });

        // sauvegarde les modifications de la grille
        post("/grid/save", (req, res) -> {
            int numSession = getSession(req);
            _Initializer.commit(_Connector.getConnection(numSession));
            return "";
        });

        // annule les modifications de la grille
        post("/grid/cancel", (req, res) -> {
            int numSession = getSession(req);
            _Initializer.rollback(_Connector.getConnection(numSession));
            GridManager gridManager = new GridManager(numSession);
            return new Gson().toJson(gridManager.getGrid());
        });

        // charge un fichier rle depuis un URL
        put("/grid/rle", (req, res) -> {
            String RLEUrl = req.body();
            int numSession = getSession(req);
            GridManager gridManager = new GridManager(numSession);
            try {
                List<CellEntity> state = GridManager.decodeRLEUrl(RLEUrl);
                gridManager.changeGrid(state);
                return new Gson().toJson(gridManager.getGrid());
            } catch (Exception e) {
                e.printStackTrace();
                res.status(500);
                return "Error: Failed to decode RLE from URL.";
            }
        });

        // vide la grille
        post("/grid/empty", (req, res) -> {
            int session = getSession(req);
            GridManager gridManager = new GridManager(session);
            gridManager.clearGrid();
            return "";
        });

        // met à jour la grille en la remplaçant par la génération suivante
        post("/grid/next", (req, res) -> {
            int numSession = getSession(req);
            GridManager gridManager = new GridManager(numSession);
            gridManager.simulateNextGeneration();
            return new Gson().toJson(gridManager.getGrid());
        });
    }

    /**
     * retourne le numéro de session
     * il y a un numéro de session différent pour chaque onglet de navigateur
     * ouvert sur l'application
     */
    public static int getSession(Request req) {
        return Integer.parseInt(req.queryParams("session"));
    }
}
